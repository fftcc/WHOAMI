<div align="center">
<img src="./logo.gif" alt="logo" width="128px" height="128px">

## About

Freak from the computer club (fftcc)

Web/NodeJS developer

OpenPGP fingerprint: CD2C086516B07B7040342426C946BC930992BBCB

OpenPGP key: [armored key](./ascii.gpg) · [binary key](./bin.gpg)

Keyoxide: https://keyoxide.org/me@ff99cc.art

Email: <a href="mailto:me@ff99cc.art">me@ff99cc.art</a>

XMPP: <a href="xmpp:fftcc@404.city">fftcc@404.city</a>

<br>

**Visit my weird websites**

[![ff99cc.art](https://img.shields.io/website?down_color=ef4484&down_message=offline&label=ff99cc.art&style=flat-square&up_color=29d29b&up_message=online&url=https%3A%2F%2Fff99cc.art)](https://ff99cc.art) [![ff99cc.neocities.org](https://img.shields.io/website?down_color=ef4484&down_message=offline&label=ff99cc.neocities.org&style=flat-square&up_color=29d29b&up_message=online&url=https%3A%2F%2Fff99cc.neocities.org)](https://ff99cc.neocities.org)

**Follow me**

[![Codeberg](https://img.shields.io/badge/Codeberg-2185D0?style=for-the-badge&logo=Codeberg&logoColor=white)](https://codeberg.org) [![GitHub](./badges/github.svg)](https://github.com/fftcc) [![Neocities](./badges/neocities.svg)](https://neocities.org/site/ff99cc) [![Mastodon](https://img.shields.io/badge/-MASTODON-573DD0?style=for-the-badge&logo=mastodon&logoColor=white)](https://fosstodon.org/@fftcc) [![Reddit](https://img.shields.io/badge/Reddit-%23FF4500.svg?style=for-the-badge&logo=Reddit&logoColor=white)](https://www.reddit.com/user/fftcc/) [![CodePen](https://img.shields.io/badge/Codepen-ffffff?style=for-the-badge&logo=codepen&logoColor=black)](https://codepen.io/ff99cc-dot-art/) [![keyoxide](./badges/keyoxide.svg)](https://keyoxide.org/me@ff99cc.art)

[![Mastodon Follow](https://img.shields.io/mastodon/follow/109766655789494947?domain=https%3A%2F%2Ffosstodon.org&style=social)](https://fosstodon.org/@fftcc) [![Reddit User Karma](https://img.shields.io/reddit/user-karma/combined/fftcc?style=social)](https://www.reddit.com/user/fftcc/) [![Stack Exchange reputation](https://img.shields.io/stackexchange/stackoverflow/r/21089914?style=social&logo=stackoverflow)](https://stackoverflow.com/users/21089914/freeeeeeee)

<br>

## FOSS projects

</div>

**Acid Theme** - One awesome dark acid theme for many apps 💅

[source code](https://codeberg.org/acidmoon/acidmoon-theme) · [website ![acid.ff99cc.art](https://img.shields.io/website?down_color=ef4484&down_message=offline&label&style=flat-square&up_color=29d29b&up_message=online&url=https%3A%2F%2Facid.ff99cc.art)](https://acid.ff99cc.art) ![license gpl-3.0](./badges/gpl.svg)

<br>

**Lazycommit** - CLI app for auto-generating commit messages. Don't want to invent a commit message? Lazycommit will automatically generate a commit message based on changes in the repository (output from git status).

[source code](https://codeberg.org/fftcc/lazycommit-cli) ![license mit](./badges/gpl.svg) ![npm version](https://img.shields.io/npm/v/lazycommit-cli?color=%23a979ff&down_color=ef4484&style=flat-square) ![npm downloads](https://img.shields.io/npm/dm/lazycommit-cli?color=%23fee14a&style=flat-square)

 <br>

**Copysite** - CLI app to create a complete local copy of a (static) site while maintaining the file structure.

[source code](https://codeberg.org/fftcc/copysite.js) ![license gpl-3.0](./badges/gpl.svg) ![npm version](https://img.shields.io/npm/v/copysite?color=%23a979ff&style=flat-square) ![npm downloads](https://img.shields.io/npm/dm/copysite?color=%23fee14a&style=flat-square)

<br>

**Own scrolls** - Tiny JS Library for scrolls.

[source code](https://codeberg.org/fftcc/own-scrolls) · [website ![own-scrolls.ff99cc.art](https://img.shields.io/website?down_color=ef4484&down_message=offline&label&style=flat-square&up_color=29d29b&up_message=online&url=https%3A%2F%2Fown-scrolls.ff99cc.art)](https://own-scrolls.ff99cc.art) ![license mit](./badges/mit.svg) ![npm version](https://img.shields.io/npm/v/own-scrolls?color=%23a979ff&style=flat-square) ![npm downloads](https://img.shields.io/npm/dm/own-scrolls?color=%23fee14a&style=flat-square)

<br>

<div align="center">
<h3>Buy an avid coffee lover a cup of coffee !</h3>

![Bitcoin](https://img.shields.io/badge/Bitcoin-ff9500?style=for-the-badge&logo=bitcoin&logoColor=white) ![Bitcoin Cash](https://img.shields.io/badge/Bitcoin%20Cash-02c28f?style=for-the-badge&logo=Bitcoin%20Cash&logoColor=white) ![Dogecoin](https://img.shields.io/badge/dogecoin-e2cc85?style=for-the-badge&logo=dogecoin&logoColor=black) ![Ethereum](https://img.shields.io/badge/Ethereum-474680?style=for-the-badge&logo=Ethereum&logoColor=white) ![Litecoin](https://img.shields.io/badge/Litecoin-A6A9AA?style=for-the-badge&logo=Litecoin&logoColor=white) ![Monero](https://img.shields.io/badge/monero-FF6600?style=for-the-badge&logo=monero&logoColor=white) ![Tether](https://img.shields.io/badge/tether-168363?style=for-the-badge&logo=tether&logoColor=white) ![Z Cash](https://img.shields.io/badge/Zcash-F4B728?style=for-the-badge&logo=zcash&logoColor=black)

</div>

<!--
<br>

[![bitcoin: bc1qwxfp6fn59na7wu2qqhw82ygqk3hwgfw3ycau6q](https://codeberg.org/fftcc/WHOAMI/raw/main/crypto/btc/btc.png)](https://codeberg.org/fftcc/WHOAMI/src/main/crypto/btc/btc.md)

[![usdt: TDaypP2FmXbBP7y37kBdrfZVZh4ek2ASGk](https://codeberg.org/fftcc/WHOAMI/raw/main/crypto/trx/trx-usdt.png)](https://codeberg.org/fftcc/WHOAMI/src/main/crypto/trx/trx.md)

[![monero: 44rgyH5aXs6KT2usDc4GhZXMNzGUmgMi1jiAGkXxsgomBLPFK7Bb68S4r4HioHYakdGr8nUKFZvC5RgXwesZZ5NQKUJ86GP](https://codeberg.org/fftcc/WHOAMI/raw/main/crypto/xmr/xmr.png)](https://codeberg.org/fftcc/WHOAMI/src/main/crypto/xmr/xmr.md)

[![ethereum: 0xA65150658ceE69De24c35546263f06A210aD4236](https://codeberg.org/fftcc/WHOAMI/raw/main/crypto/eth/eth.png)](https://codeberg.org/fftcc/WHOAMI/src/main/crypto/eth/eth.md)

[![litecoin: LS61mxdRagWYSgZXHUbfpqiBrV6pWDyFUT](https://codeberg.org/fftcc/WHOAMI/raw/main/crypto/ltc/ltc.png)](https://codeberg.org/fftcc/WHOAMI/src/main/crypto/ltc/ltc.md)

[**More coin...**](https://codeberg.org/fftcc/WHOAMI/src/main/buy-me-a-coffee.md)
-->
